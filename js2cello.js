/*
    The Language
    Copyright (C) 2018, 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
const jsparser=require('@babel/parser')

const sha1=((shajs)=>(x)=>(new shajs.sha1()).update(x).digest('hex'))(require('sha.js'))
function mktmpname(x){
    return sha1(x).slice(0,6)
}

const assert_strict = require('assert').strict
function assert_equal(x,y,msg=()=>null){ // msg懶惰求值。
    assert_strict.equal(x,y,{ toString() { return msg(); } })
}
function assert_notEqual(x,y,msg=()=>null){
    assert_strict.notEqual(x,y,{ toString() { return msg(); } })
}
function assert_fail(msg=null){
    assert_strict.ok(false,msg)
}
function assert_true(x,msg=()=>null){
    assert_strict.ok(x,{ toString() { return msg(); } })
}

function object_copy(o){
    return Object.assign({},o)
}

function print_loc_of(x){
    const {loc:{ start: { line: loc_start_line, column: loc_start_column }, end: { line: loc_end_line, column: loc_end_column } }}=x
    return `((start (line ${loc_start_line}) (column ${loc_start_column})) (end (line ${loc_end_line}) (column ${loc_end_column})))`
}

function raw_jsid2cello(x){return x}
function jsid2cello(x,env){
    assert_equal(env.type, 'Env')
    if(x in env.nonlocals){
	env.used_upvals[x]=true
    }
    return raw_jsid2cello(x)
}

function pre(x){
    return jsparser.parse(x)
}
exports.pre=pre

function table2f(ta){
    return (x,...xs)=>{
	assert_true(x.type in ta,()=>`Unsupported node "${x.type}" at ${print_loc_of(x)}`)
	return ta[x.type](x,...xs)
    }
}

function um_emit(f){
    return (x,env)=>{
	const new_env=object_copy(env)
	let result=""
	new_env.emit=(v)=>{result+=v}
	f(x,new_env)
	return result
    }
}

function transpile(x){
    const i=mktmpname(x)
    const prog_init='init_'+i
    const prog_exports='exports_'+i
    let head=`var ${prog_exports}=NULL;\n`
    let global=""
    const result=um_emit(evalFile)(x,{type:'Env',
				      emit:null,
				      head_emit:(v)=>{head+=v},
				      global_emit:(v)=>{global+=v},
				      prog:{init:prog_init,exports_var:prog_exports},
				      used_upvals:{},// Set<JSId>
				      used_locals:{},// Set<JSId>
				      nonlocals:{},// Set<JSId>
				     })
    return {init:prog_init,exports_var:prog_exports, code:head+global+result}
}
exports.transpile=transpile

function env_addlocal(env,x){
    assert_equal(env.type, 'Env')
    assert_equal(env.used_locals[x],undefined)
    env.used_locals[x]=true
}
function evalFile(x,env){
    assert_equal(x.type, 'File')
    return evalProgram(x.program,env)
}
function evalProgram(x,env){
    assert_equal(x.type, 'Program')
    env.emit(`void ${env.prog.init}() {\n`)
    env.emit(`if(NULL==${env.prog.exports_var}){\n`)
    env.emit(`${env.prog.exports_var} = new(Table, String, Ref);\n`)
    for(const line of x.body){
	evalStatement(line,env)
    }
    env.emit(`}\n`)
    env.emit(`}\n`)
}
const evalStatement_r=table2f({
    ExpressionStatement:evalExpressionStatement,
    VariableDeclaration:evalVariableDeclaration,
})
function evalStatement(...xs){return evalStatement_r(...xs)}

function evalExpressionStatement(x,env){
    assert_equal(x.type, 'ExpressionStatement')
    evalExpression(x.expression,env)
    env.emit(`;\n`)
}
function evalVariableDeclaration(x,env){
    assert_equal(x.type, 'VariableDeclaration')
    assert_true(({const:true,let:true})[x.kind])
    assert_equal(x.declarations.length,1)
    const [declaration]=x.declarations
    assert_equal(declaration.type,'VariableDeclarator')
    const {id, init}=declaration
    assert_equal(id.type, 'Identifier')
    env.emit(`var `)
    env_addlocal(env, id.name)
    env.emit(jsid2cello(id.name,env))
    env.emit(` = `)
    evalExpression(init,env)
    env.emit(`;\n`)
}

const evalExpression_r=table2f({
    AssignmentExpression:evalBinaryExpressionAssignmentExpression,
    Identifier:(x,env)=>env.emit(jsid2cello(x.name,env)),
    NumericLiteral:(x,env)=>env.emit(`$F(${x.extra.rawValue.toString()})`),
    BinaryExpression:evalBinaryExpressionAssignmentExpression,
})
function evalExpression(...xs){return evalExpression_r(...xs)}

function evalBinaryExpressionAssignmentExpression(x,env){
    assert_true(x.type==='BinaryExpression'||x.type==='AssignmentExpression')
    const {operator, left, right}=x
    env.emit(`(`)
    evalExpression(left,env)
    const operators={
	'=':'=',
	'+':'+',
	'-':'-',
	'*':'*',
	'/':'/',
    }
    assert_true(operator in operators)
    env.emit(operators[operator])
    evalExpression(right,env)
    env.emit(`)`)
}
